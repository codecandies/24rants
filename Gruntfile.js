// Wrapper function with one parameter
module.exports = function( grunt ) {
    'use strict';

    // local variables
    var project = {
        name: '<%= pkg.name %>-<%= pkg.version%>',
        sourceDir: 'src/',
        distDir: 'dist/',
        port: '8001',
        tasks: {
            dist: [
                'clean',
                'sass',
                'postcss:dist',
                'eslint',
                'babel',
                'htmlmin:dist',
                'svgmin',
                'imagemin',
                'copy:dist',
                'responsive_images'
            ],
            dev: [
                'clean',
                'sass',
                'postcss:dev',
                'eslint',
                'babel',
                'htmlmin:dev',
                'svgmin',
                'copy:dist',
                'responsive_images'
            ],
            build: [
                'clean',
                'sass',
                'postcss:dist',
                'eslint',
                'babel',
                'htmlmin:dist',
                'svgmin',
                'imagemin',
                'responsive_images',
                'uglify',
                'copy'
            ] 
        }
    };

    require( 'load-grunt-tasks' )( grunt );

    grunt.initConfig({

        // read from package.json
        pkg: grunt.file.readJSON( 'package.json' ),

        clean: {
            folder: [ project.distDir ]
        },

        copy: {
            dist: {
                files: [{
                        expand: true,
                        cwd: project.sourceDir + 'fonts/',
                        src: '**/*',
                        dest: project.distDir + 'fonts/'
                    }, {
                        expand: true,
                        cwd: project.sourceDir + 'svg/min',
                        src: '**/*',
                        dest: project.distDir + 'svg/'
                    }, {
                        expand: true, 
                        cwd: project.sourceDir + 'img',
                        src: '**/*', 
                        dest:  project.distDir + 'img/'
                    }
                ]
            },
            build: {
                files: [
                    {
                        expand: true,
                        cwd: project.sourceDir + 'wordpress/',
                        src: '**/*',
                        dest: project.distDir + 'wordpress/'
                    }, {
                        expand: true,
                        cwd: project.distDir + 'img/',
                        src: '**/*',
                        dest: project.distDir + 'wordpress/img/'
                    }, {
                        expand: true,
                        cwd: project.sourceDir + 'fonts/',
                        src: '**/*',
                        dest: project.distDir + 'wordpress/fonts/'
                    }, {
                        expand: true,
                        cwd: project.sourceDir + 'svg/min',
                        src: '**/*',
                        dest: project.distDir + 'wordpress/svg/'
                    }, {
                        expand: true, 
                        cwd: project.distDir + 'css',
                        src: '**/*', 
                        dest:  project.distDir + 'wordpress/css/'
                    }, {
                        expand: true, 
                        cwd: project.distDir + 'js',
                        src: '**/*', 
                        dest:  project.distDir + 'wordpress/js/'
                    }
                ]
            }
        },

        uglify: {
            options: {
                mangle: true
            },
            dist: {
                files: {
                    'dist/wordpress/js/app.min.js': ['dist/js/app.js']
                }
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: project.sourceDir,
                    src: '**/*.html',
                    dest: project.distDir
                }]
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: project.sourceDir,
                    src: '**/*.html',
                    dest: project.distDir
                }]
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: project.sourceDir + 'sass/',
                    src: '*.sass',
                    dest: project.distDir + 'css/',
                    ext: '.css'
                }]
            }
        },

        postcss: {
            options: {
                map: {
                    inline: false,
                    annotation: 'dist/css/maps/'
                }
            },
            dev: {
                options: {
                    processors: [
                        require( 'pixrem' )(),
                        require( 'autoprefixer' )({ browsers: 'last 2 versions' })
                    ]
                },
                files: [{
                    expand: true,
                    cwd: project.distDir + 'css/',
                    src: '**/*.css',
                    dest: project.distDir + 'css/'
                }]
            },
            dist: {
                options: {
                    map: false,
                    processors: [
                        require( 'pixrem' )(),
                        require( 'autoprefixer' )(),
                        require( 'cssnano' )()
                    ]
                },
                files: [{
                    expand: true,
                    cwd: project.distDir + 'css/',
                    src: '**/*.css',
                    dest: project.distDir + 'css/'
                }]
            }
        },

        eslint: {
            target: project.sourceDir + 'js/app.js'
        },

        babel: {
            options: {
                sourceMap: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: project.sourceDir + 'js/',
                    src: '**/*.js',
                    dest: project.distDir + 'js/'
                }]
            }
        },

        svgmin: {
            options: {
                plugins: [{
                    removeViewBox: false
                }]
            },
            symbols: {
                expand: true,
                cwd: project.sourceDir + 'svg',
                src: [ '*.svg' ],
                dest: project.sourceDir + 'svg/min/'
            }
        },

        responsive_images: {
            options: {
                engine: 'im'
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: project.sourceDir + 'img',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: project.distDir + 'img/'
                }]
            }
        },

        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}]
                },
                files: [{
                    expand: true,
                    cwd: project.sourceDir + 'svg/min',
                    src: ['**/*.svg'],
                    dest: project.distDir + 'svg/'
                }]
            }
        },

        connect: {
            server: {
                options: {
                    port: project.port,
                    base: project.distDir,
                    hostname: 'localhost',
                    livereload: true,
                    open: {
                        target: 'http://localhost:' + project.port + '/index.html'
                    }
                }
            }
        },

        watch: {
            sass: {
                files: ['src/**/*.sass'],
                tasks: ['sass']
            },
            htmlmin: {
                files: ['src/**/*.html'],
                tasks: ['htmlmin']
            },
            copy_fonts: {
                files: ['src/fonts/*'],
                tasks: ['copy:fonts']
            },
            copy_dist: {
                files: ['src/img/*'],
                tasks: ['copy:dist']
            },
            responsive_images: {
                files: ['src/img/**/*'],
                tasks: ['responsive_images']
            },
            postcss: {
                files: [project.distDir + 'css/*.css'],
                tasks: ['postcss:dev']
            },
            js: {
                files: [ project.sourceDir + 'js/**/*.js' ],
                tasks: [ 'eslint', 'babel' ]
            },
            livereload: {
                // This target doesn't run any tasks
                // But when a file in `dist/css/*` is edited it will trigger the live reload
                // So when compass compiles the files, it will only trigger live reload on
                // the css files and not on the scss files
                files: [ project.distDir + 'css/screen.css', project.distDir + 'js/app.js' ],
                options: {
                    //livereload: true
                }
            },
            symbols: {
                files: [ '<%= svgmin.symbols.cwd %>/*.svg' ],
                tasks: [ 'svgmin:symbols' ]
            },
            copy_svg: {
                files: ['src/svg/min/*'],
                tasks: ['copy:svg']
            },
            config: {
                files: [
                    project.sourceDir + '.jscsrc',
                    project.sourceDir + '.jshintrc',
                    project.sourceDir + '.babelrc',
                    project.sourceDir + 'Gruntfile.js'
                ],
                options: {
                    reload: true
                }
            }
        }

    });

    grunt.registerTask( 'default', project.tasks.dev );
    grunt.registerTask( 'dev', project.tasks.dev );
    grunt.registerTask( 'dist', project.tasks.dist );
    grunt.registerTask( 'build', project.tasks.build );
    grunt.registerTask( 'server', [ 'connect', 'watch' ] );
};
