# 24 Rants

## Der Rantventskalender im Couchblog
Der Kalender soll in diesem Jahr ein echter Adventskalender sein, mit dieser zugegeben *irren* Idee startete ich Ende Oktober in der Entwicklung. Kollegen erinnerten sich kollektiv an Zeiten bei Agenturen, wo regelmäßig kurz vor der Weihnachtszeit Adventskalender für Webseiten oder Flash-Animationen von Schnee und ähnlichem Gedöns zur Einbindung auf hunderten Kundensites, in Auftrag gegeben wurde. Kurz vor knapp, aber im Juli entwickelt ja kein Mensch einen Adventskalender! Das wäre ja, als wenn man ein Tippspiel ein Jahr vor der nächsten WM bauen würde. Wo kämen wir da hin? Eben.

So gesehen ist Ende Oktober noch recht langfristig gedacht, wir hatten ja nichts damals. Nun haben wir gerade noch so Mitte November und das Projekt nimmt so konkrete Formen an, dass ich an seine rechtzeitige Fertigstellung glaube. Ohne rot zu werden. Was mir den Spaß fast ein wenig verdorben hat, ist die buggy Umsetzung der 3-D-Animationen in Firefox, fixes Hintergrundbild und öffnende Fensterchen waren für die Echse einfach ein Effekt zu viel. Ich empfehle sich das Werk in einem Webkit-Browser zumindest mal anzuschauen. ;) Die Startseite ist schon ein paar Tage fertig, sowohl der Entwurf, als auch die Wordpress-Template-Datei, die es brauchen wird, um im Blog untergebracht zu werden. Die Artikelseite steht zumindest prototypisch, hier fehlt das Template noch, aber hey…

Wie das sich gehört, habe ich unterwegs Flausen aus meinem Kopf entfernt, die an dieser Stelle eine auf Vue.js gestützte Onepage-Webapp gesehen haben. Stattdessen ist wieder alles mit der Hand gestreckt und der Content wird, ganz altmodisch, aus dem Wordpress kommen. Nun ja, die Basis ist ja da, vielleicht kann man ja nächstes Jahr darauf weiter aufbauen. Oder eben nicht, das wäre nur konsequent. So, ich muss dann mal los… noch ungefähr 23 Rants aufschreiben!

## Wordpress
Das ganze ist als Child-Rumpf-Theme des  im [Couchblog](https://couchblog.de) genutzten Twenty-Fifteen-Theme umgesetzt, mit einem Page-Template und einem Artikel-Template.

## How to build
- Bitte schön das Grunt Command Line Interface installieren  
`$> npm install -g grunt-cli`
- Dann dieses Repo (am liebsten) branchen und auschecken
- Und jetzt ratet mal… `npm install` you go…
- Entwicklen mit `grunt server` (http://localhost:8001/index.html sollte sich automagisch öffnen)
- bauen mit `grunt` oder `grunt dev` oder für Production: `grunt dist`
- Wordpress Theme files bauen mit `grunt build`
