/*global window, document */
/*eslint no-console: "off" */
'use strict';

(function () {
  const days_wrapper = document.querySelector('.days')

  let handleFirstTab = event => {
    if (event.keyCode === 9) {
      document.body.classList.add('user-tabbing')
      window.removeEventListener('keydown', handleFirstTab)
    }
  }

  let shuffleCalendar = (elements, elementClassName) => {
    const shuffleArray = arr => arr.sort(() => Math.random() - 0.5)
    const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]
    const adventdays = shuffleArray(days)
    for (let elem of elements) {
      const number = adventdays.shift() || 24
      elem.classList.add(elementClassName + '--' + number)
    }
  }

  let hide = () => {
      [].forEach.call( document.querySelectorAll( '.door[aria-expanded=true]' ), ( open ) => {
          const behind = open.nextElementSibling
          open.setAttribute('aria-expanded', false)
          if (behind) {
              behind.setAttribute('aria-hidden', 'true')
          }
      })
  };

  shuffleCalendar(document.querySelectorAll('.day'), 'order')
  shuffleCalendar(document.querySelectorAll('.item'), 'item')

  window.addEventListener('keydown', handleFirstTab)

  if (days_wrapper) {
    days_wrapper.addEventListener('click', event => {
      const target = event.target
      const targetSibling = target.nextElementSibling
      // always hide on click
      hide()
      if ( target.getAttribute('role') === 'button' ) {
        event.preventDefault()
        // open self
        target.setAttribute('aria-expanded', true)
        targetSibling.setAttribute('aria-hidden', false)
        targetSibling.focus()
      }
    })
    days_wrapper.addEventListener('keyup', event => {
      const target = event.target
      if ( target.getAttribute('role') === 'button' ) {
        if ( event.keyCode === 13 || event.keyCode === 32 ) {
          event.preventDefault();
          target.click();
        }
      }
    })
  }

}());
