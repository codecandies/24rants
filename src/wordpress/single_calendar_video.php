<?php
/*
 * Template Name: Calendar Video
 * Template Post Type: post
 * The template for displaying videos on a calendar page
 *
 * @package WordPress
 * @subpackage 2015
 * @since 2015 1.0
 */
	get_template_part( 'calendar', 'header' );
	$the_id = false;
?>
<main class="main">
	<div class="wrapper">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php $the_id = get_the_ID(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<h2 class="entry-subtitle"><?php echo $post->post_excerpt; ?></h2>
			</header><!-- .entry-header -->
			<div class="entry-content entry-content--fullwidth">
				<?php the_content(); ?>
			</div>
			<footer class="entry-footer">
				<?php $twitter = get_the_author_meta( 'twitter', $post->post_author ); ?>
				<p>
					<?php if ($twitter): ?>
						<a href="<?php echo esc_url('https://twitter.com/' . $twitter); ?>"><?php echo get_the_author(); ?></a>
					<?php else: ?>
						<?php echo get_the_author(); ?>
					<?php endif; ?>
					am <?php echo get_the_date( 'j. F Y' ); ?></p>
			</footer><!-- .entry-footer -->
	<?php 
		endwhile;
		wp_reset_postdata();
		$display_post_types = array('publish');
		if ( is_preview() ) {
			$display_post_types = array('draft', 'future');
		}
		// The Query
		$args = array( 
			'category_name' => 'rantventskalender+2017',
			'post_status' => $display_post_types,
			'nopaging' => true,
			'order' => 'ASC'
		);
		$the_query = new WP_Query( $args );
	?>
			<nav class="calendar">
				<?php for( $i = 0; $i < 24; $i++): ?>
					<?php if( $the_query->posts[$i] ): ?>
					<?php
						global $post;
						$post = $the_query->posts[$i];
						setup_postdata($post);
					?>
					<a class="item <?php if($the_id == get_the_ID()): ?>item--actual<?php endif; ?>" title="<?php echo esc_attr( get_the_title() ); ?>" href="<?php echo esc_url( get_permalink() ); ?>"><?php echo $i + 1; ?></a>
					<?php else: ?>
						<span class="item"><?php echo $i +1; ?></span>
					<?php endif; ?>
				<?php endfor; wp_reset_postdata(); ?>
			</nav>
			<nav class="textlink">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">Zurück zur Startseite</a>
			</nav>
		</article>
	</div>
</main>
<?php
	get_template_part( 'calendar', 'footer' );
