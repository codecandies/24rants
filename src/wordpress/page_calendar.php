<?php
/**
 * Template Name: Calendar Page
 * 
 * The template for displaying the advents calendar page
 *
 * @package WordPress
 * @subpackage 2015
 * @since 2015 1.0
 */
	get_template_part( 'calendar', 'header' );
	// Show drafts and futures in draft modus
	$display_post_types = array('publish');
	if ( is_preview() ) {
		$display_post_types = array('draft', 'future');
	}
	// The Query
	$args = array( 
		'category_name' => 'rantventskalender+2017',
		'post_status' => $display_post_types,
		'nopaging' => true,
		'order' => 'ASC'
	);
	$the_query = new WP_Query( $args );
	?>
	<main class="main">
		<div class="wrapper">
			<div class="days">
			<?php for( $i = 0; $i < 24; $i++): ?>
				<article class="day">
					<?php if( $the_query->posts[$i] ):
							global $post;
							$post = $the_query->posts[$i];
							setup_postdata($post);
						?>
						<a 
							href="<?php the_permalink(); ?>" 
							class="day__door door door--<?php echo $i+1; ?>" 
							id="door-<?php echo $i+1; ?>" 
							role="button" 
							title="<?php echo $i+1; ?>. Dezember"
							aria-controls="link-<?php echo $i+1; ?>"
						>
							<?php echo $i+1; ?>
						</a>
						<a 
							href="<?php the_permalink(); ?>" 
							class="day__link link" 
							id="link-<?php echo $i+1; ?>" 
							aria-hidden="true"
							title="<?php the_title(); ?> - <?php echo $post->post_excerpt; ?>"
						>
							<h1 class="link__title"><?php the_title(); ?></h1>
							<h2 class="link__subtitle"><?php echo $post->post_excerpt; ?></h2>
						</a>
					<?php else: ?>
						<div class="day__door door door--<?php echo $i+1; ?>" id="door-<?php echo $i+1; ?>"><?php echo $i+1; ?></div>
					<?php endif; ?>
				</article>
			<?php endfor;
			/* Restore original Post Data */
			wp_reset_postdata();
			?>
			</div>
		</div>
	</main>
<?php
get_template_part( 'calendar', 'footer' );

