	<footer class="footer">
		<div class="footer__wrapper wrapper">
			<p>© 2017 Nico Brünjes @ <a href="https://couchblog.de/">couchblog.de</a>. Es gelten die allgemeinen <em>Gefechts</em>bedingungen und dieses <a href="https://couchblog.de/impressum/">Impressum</a>, wenn es denn sein soll. Grafiken von TheHungryJPEG, Niederlande.</p>
			<details class="footer__details">
				<summary class="footer__summary">Weiterlesen…</summary>
				<p>Die Grafiken auf dieser und den Folgeseiten sind ausnahmsweise einmal <strong>nicht</strong> unter Creative Commons Lizenz lizensiert, sondern stattdessen sind alle Rechte vorbehalten, insbesondere um von dem kosmischen Weihnachtsmann noch T-Shirts zu drucken, falls mir der Sinn danach steht.</p>
				<p>Das Jahr geht mal wieder zu Ende und auch dieses war ein Gutes, trotz aller Katastrophen, Wahlen, Katastrophenwahlen und dem Umstand, dass ein Drittel meiner Idole verstorben ist. Ich danke an dieser Stelle allen Leuten, die Schuld daran sind…, dass mir meine Arbeit soviel Spass macht, nebenbei noch so einen Blödsinn wie diesen hier zu veranstalten. Ihr rockt sowas von. Ein extra dickes Danke Schön! haben sich verdient: Suzanne, Thomas, Tobi, Paul, Moritz, Anika, Ron, Arne, Ben und und und…</p>
				<p>Danke Euch allen! Frohes Jahresendfest und ein gutes neues Jahr!</p>
				<p>Look Ma, I used &lt;details&gt;!</p>
			</details>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.min.js"></script>
</body>
</html>
