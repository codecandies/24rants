<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title>24 Rants - Der Rantventskalender - Ausgabe 2017</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
	<meta http-equiv="x-dns-prefetch-control" content="on">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/screen.css" media="screen">
	<?php wp_head(); ?>
</head>
<body class="body<?php if(is_single()): ?>  body--article<? endif; ?>">
	<header class="header">
		<h1 class="header__logo"><span class="header__text">24 Rants</span></h1>
		<h2 class="header__subline">Der <em>Rant</em>ventskalender<br> im <a href="https://couchblog.de">Couchblog</a></h2>
	</header>
