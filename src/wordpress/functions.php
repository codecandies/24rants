<?php
	function twentyfifteen_child_styles() {
		wp_deregister_style( 'twentyfifteen-style');
		wp_register_style('twentyfifteen-style', get_template_directory_uri(). '/style.css');
		wp_enqueue_style('twentyfifteen-style', get_template_directory_uri(). '/style.css');
		wp_enqueue_style( 'childtheme-style', get_stylesheet_directory_uri().'/style.css', array('twentyfifteen-style') );
	}
	add_action( 'wp_enqueue_scripts', 'twentyfifteen_child_styles' );

	/**
	 * Remove all java scripts.
	 */
	function se_remove_all_scripts() {
	    global $wp_scripts;
	    if ( is_page_template( 'page_calendar.php' ) || is_page_template( 'single_calendar.php' ) || is_page_template( 'single_calendar_video.php' ) ) {
	        $wp_scripts->queue = array();
	    }
	}

	add_action( 'wp_print_scripts', 'se_remove_all_scripts', 99 );

	/**
	 * Remove all style sheets.
	 */
	function se_remove_all_styles() {
	    global $wp_styles;
	    if ( is_page_template( 'page_calendar.php' ) || is_page_template( 'single_calendar.php' ) || is_page_template( 'single_calendar_video.php' ) ) {
	        $wp_styles->queue = array();
	    }
	}

	add_action( 'wp_print_styles', 'se_remove_all_styles', 99 );
